package com.marioea.mongo.gui;

import com.marioea.mongo.base.Juguete;
import com.marioea.mongo.base.Jugueteria;
import com.marioea.mongo.base.Proveedor;
import com.marioea.mongo.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Mario Escos
 * @author
 */

/**
 * Clase Modelo
 */


public class Modelo {

    /**
     * Creamos las distintas colecciones que vamos a utilizar
     */
    private final static String COLECCION_JUGUETE = "Juguete";
    private final static String COLECCION_JUGUETERIA = "Jugueteria";
    private final static String COLECCION_PROVEEDOR = "Proveedor";
    private final static String DATABASE = "AlmacenJugueteria";

    /**
     * creamos el client de mongo, la base de datos y las colecciones de mongo
     */
    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection coleccionJuguete;
    private MongoCollection coleccionJugueteria;
    private MongoCollection coleccionProveedor;

    /**
     * Método conectar(), conectas con la base de datos
     */
    public void conectar() {
        client = new MongoClient();
        baseDatos = client.getDatabase(DATABASE);
        coleccionJuguete = baseDatos.getCollection(COLECCION_JUGUETE);
        coleccionJugueteria = baseDatos.getCollection(COLECCION_JUGUETERIA);
        coleccionProveedor = baseDatos.getCollection(COLECCION_PROVEEDOR);
    }

    /**
     * Método desconectar(), para desconectar la base de datos
     */
    public void desconectar() {
        client.close();

    }

    /**
     * Método guardarJuguete(), para guardar juguetes en la base de datos
     * con todos los datos de un juguete
     *
     * @param unJuguete de tipo Juguete
     */
    public void guardarJuguete(Juguete unJuguete) {
        coleccionJuguete.insertOne(jugueteToDocument(unJuguete));

    }

    /**
     * Método guardarJugueteria(), para guardar una Jugueteria en la base de datos
     * con todos los datos de una jugueteria
     * @param unaJugueteria de tipo Jugueteria
     */
    public void guardarJugueteria(Jugueteria unaJugueteria) {
        coleccionJugueteria.insertOne(jugueteriaToDocument(unaJugueteria));

    }

    /**
     * Método guardarProveedor(), para guardar un Proveedor en la base de datos
     * con todos los datos de un proveedor
     * @param unProveedor de tipo Proveedor
     */
    public void guardarProveedor(Proveedor unProveedor) {
        coleccionProveedor.insertOne(proveedorToDocument(unProveedor));

    }

    /**
     * Método getJuguete(), lista juguetes
     *
     * @return lista
     */
    public List<Juguete> getJuguete() {
        ArrayList<Juguete> lista = new ArrayList<>();

        Iterator<Document> it = coleccionJuguete.find().iterator();
        while (it.hasNext()) {
            lista.add(documentToJuguete(it.next()));
        }
        return lista;
    }

    /**
     * Método getJugueteria(), lista Jugueterias
     *
     * @return lista
     */
    public List<Jugueteria> getJugueteria() {
        ArrayList<Jugueteria> lista = new ArrayList<>();

        Iterator<Document> it = coleccionJugueteria.find().iterator();
        while (it.hasNext()) {
            lista.add(documentToJugueteria(it.next()));
        }
        return lista;
    }
    /**
     * Método getProveedor(), lista proveedores
     *
     * @return lista
     */
    public List<Proveedor> getProveedor() {
        ArrayList<Proveedor> lista = new ArrayList<>();

        Iterator<Document> it = coleccionProveedor.find().iterator();
        while (it.hasNext()) {
            lista.add(documentToProveedor(it.next()));
        }
        return lista;
    }


    /**
     * Método getJugueteSimple(),listo los juguetes atendiendo a 1 criterio basados en una expresion regular
     *
     * @param text de tipo String
     * @return lista
     */
    public List<Juguete> getJugueteSimple(String text) {
        ArrayList<Juguete> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("codigoJuguete", new Document("$regex", "/*" + text + "/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionJuguete.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToJuguete(iterator.next()));
        }

        return lista;
    }
    /**
     * Método getJuguete(),listo los juguetes atendiendo a 2 criterios basados en  expresiones regulares
     *
     * @param text de tipo String
     * @return lista
     */
    public List<Juguete> getJuguete(String text) {
        ArrayList<Juguete> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombreJuguete", new Document("$regex", "/*" + text + "/*")));
        listaCriterios.add(new Document("marcaJuguete", new Document("$regex", "/*" + text + "/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionJuguete.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToJuguete(iterator.next()));
        }

        return lista;
    }

    /**
     * Método getJugueteriaSimple(),listo las jugueterias atendiendo a 1 criterio basados en una expresion regular
     *
     * @param text de tipo String
     * @return lista
     */
    public List<Jugueteria> getJugueteriaSimple(String text) {
        ArrayList<Jugueteria> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("paisJugueteria", new Document("$regex", "/*" + text + "/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionJugueteria.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToJugueteria(iterator.next()));
        }

        return lista;
    }
    /**
     * Método getJugueteria(),listo las jugueterias atendiendo a 2 criterios basados en  expresiones regulares
     *
     * @param text de tipo String
     * @return lista
     */
    public List<Jugueteria> getJugueteria(String text) {
        ArrayList<Jugueteria> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombreJugueteria", new Document("$regex", "/*" + text + "/*")));
        listaCriterios.add(new Document("ciudadJugueteria", new Document("$regex", "/*" + text + "/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionJugueteria.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToJugueteria(iterator.next()));
        }

        return lista;
    }
    /**
     * Método getProveedorSimple(),listo las proveedores atendiendo a 1 criterio basados en una expresion regular
     *
     * @param text de tipo String
     * @return lista
     */
    public List<Proveedor> getProveedorSimple(String text) {
        ArrayList<Proveedor> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("ciudadProveedor", new Document("$regex", "/*" + text + "/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionProveedor.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToProveedor(iterator.next()));
        }

        return lista;
    }
    /**
     * Método getProveedor(),listo las jugueterias atendiendo a 2 criterios basados en  expresiones regulares
     *
     * @param text de tipo String
     * @return lista
     */
    public List<Proveedor> getProveedor(String text) {
        ArrayList<Proveedor> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("codigoProveedor", new Document("$regex", "/*" + text + "/*")));
        listaCriterios.add(new Document("paisProveedor", new Document("$regex", "/*" + text + "/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionProveedor.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToProveedor(iterator.next()));
        }

        return lista;
    }

    /**
     * Método jugueteToDocument() de tipo Document
     *
     * @param unJuguete de tipo Juguete
     * @return documento
     */
    public Document jugueteToDocument(Juguete unJuguete) {
        Document documento = new Document();
        documento.append("codigoJuguete", unJuguete.getCodigoJuguete());
        documento.append("nombreJuguete", unJuguete.getNombreJuguete());
        documento.append("descripcionJuguete", unJuguete.getDescripcionJuguete());
        documento.append("marcaJuguete", unJuguete.getMarcaJuguete());
        documento.append("precioJuguete", unJuguete.getPrecioJuguete());
        documento.append("fechaCreacionJuguete", Util.formatearFecha(unJuguete.getFechaCreacionJuguete()));

        return documento;
    }

    /**
     * Método jugueteriaToDocument() de tipo Document
     *
     * @param unaJugueteria de tipo Jugueteria
     * @return documento
     */
    public Document jugueteriaToDocument(Jugueteria unaJugueteria) {
        Document documento = new Document();
        documento.append("nombreJugueteria", unaJugueteria.getNombreJugueteria());
        documento.append("ciudadJugueteria", unaJugueteria.getCiudadJugueteria());
        documento.append("paisJugueteria", unaJugueteria.getPaisJugueteria());
        documento.append("telefonoJugueteria", unaJugueteria.getTelefonoJugueteria());
        return documento;
    }
    /**
     * Método proveedorToDocument() de tipo Document
     *
     * @param unProveedor de tipo Proveedor
     * @return documento
     */
    public Document proveedorToDocument(Proveedor unProveedor) {
        Document documento = new Document();
        documento.append("codigoProveedor", unProveedor.getCodigoProveedor());
        documento.append("paisProveedor", unProveedor.getPaisProveedor());
        documento.append("ciudadProveedor", unProveedor.getCiudadProveedor());
        documento.append("direccionProveedor", unProveedor.getDireccionProveedor());
        documento.append("cantidadProveedor", unProveedor.getCantidadProveedor());
        documento.append("telefonoProveedor", unProveedor.getTelefonoProveedor());
        return documento;
    }

    /**
     * Método documentToJuguete(), registro de las filas de juguete
     *
     * @param document de tipo Document
     * @return unJuguete
     */
    public Juguete documentToJuguete(Document document) {
        Juguete unJuguete = new Juguete();
        unJuguete.setId(document.getObjectId("_id"));
        unJuguete.setCodigoJuguete(document.getString("codigoJuguete"));
        unJuguete.setNombreJuguete(document.getString("nombreJuguete"));
        unJuguete.setDescripcionJuguete(document.getString("descripcionJuguete"));
        unJuguete.setMarcaJuguete(document.getString("marcaJuguete"));
        unJuguete.setPrecioJuguete(document.getDouble("precioJuguete"));
        unJuguete.setFechaCreacionJuguete(Util.parsearFecha(document.getString("fechaCreacionJuguete")));

        return unJuguete;
    }

    /**
     * Método documentToJugueteria(), registro de las filas de jugueteria
     *
     * @param document de tipo Document
     * @return unaJugueteria
     */
    public Jugueteria documentToJugueteria(Document document) {
        Jugueteria unaJugueteria = new Jugueteria();
        unaJugueteria.setId(document.getObjectId("_id"));
        unaJugueteria.setNombreJugueteria(document.getString("nombreJugueteria"));
        unaJugueteria.setCiudadJugueteria(document.getString("ciudadJugueteria"));
        unaJugueteria.setPaisJugueteria(document.getString("paisJugueteria"));
        unaJugueteria.setTelefonoJugueteria(document.getString("telefonoJugueteria"));
        return unaJugueteria;
    }
    /**
     * Método documentToProveedor(), registro de las filas de proveedor
     *
     * @param document de tipo Document
     * @return unProveedor
     */
    public Proveedor documentToProveedor(Document document) {
        Proveedor unProveedor = new Proveedor();
        unProveedor.setId(document.getObjectId("_id"));
        unProveedor.setCodigoProveedor(document.getString("codigoProveedor"));
        unProveedor.setPaisProveedor(document.getString("paisProveedor"));
        unProveedor.setCiudadProveedor(document.getString("ciudadProveedor"));
        unProveedor.setDireccionProveedor(document.getString("direccionProveedor"));
        unProveedor.setCantidadProveedor(document.getInteger("cantidadProveedor"));
        unProveedor.setTelefonoProveedor(document.getString("telefonoProveedor"));
        return unProveedor;
    }

    /**
     * Método modificarJuguete(), sirve para modificar juguetes
     *
     * @param unJuguete de Juguete
     */
    public void modificarJuguete(Juguete unJuguete) {
        coleccionJuguete.replaceOne(new Document("_id", unJuguete.getId()), jugueteToDocument(unJuguete));
    }

    /**
     * Método modificarJugueteria(), sirve para modificar jugueterias
     *
     * @param unaJugueteria de Jugueteria
     */
    public void modificarJugueteria(Jugueteria unaJugueteria) {
        coleccionJugueteria.replaceOne(new Document("_id", unaJugueteria.getId()), jugueteriaToDocument(unaJugueteria));
    }
    /**
     * Método modificarProveedor(), sirve para modificar proveedores
     *
     * @param unProveedor de Proveedor
     */
    public void modificarProveedor(Proveedor unProveedor) {
        coleccionProveedor.replaceOne(new Document("_id", unProveedor.getId()), proveedorToDocument(unProveedor));
    }
    /**
     * Método borrarJuguete(), para borrar un juguete
     *
     * @param unJuguete de Juguete
     */
    public void borrarJuguete(Juguete unJuguete) {
        coleccionJuguete.deleteOne(jugueteToDocument(unJuguete));
    }

    /**
     * Método borrarJugueteria(), para borrar una jugueteria
     *
     * @param unaJugueteria de Jugueteria
     */
    public void borrarJugueteria(Jugueteria unaJugueteria) {
        coleccionJugueteria.deleteOne(jugueteriaToDocument(unaJugueteria));

    }
    /**
     * Método borrarProveedor(), para borrar un Proveedor
     *
     * @param unProveedor de Proveedor
     */
    public void borrarProveedor(Proveedor unProveedor) {
        coleccionProveedor.deleteOne(proveedorToDocument(unProveedor));

    }
}
