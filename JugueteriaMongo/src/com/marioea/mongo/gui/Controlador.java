package com.marioea.mongo.gui;

import com.marioea.mongo.base.Juguete;
import com.marioea.mongo.base.Jugueteria;
import com.marioea.mongo.base.Proveedor;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.util.List;

/**
 * Clase Controlador
 * @author Mario Escos
 * @see java.awt.event.ActionListener
 * @see java.awt.event.KeyListener
 * @see javax.swing.event.ListSelectionListener
 */
public class Controlador implements ActionListener, KeyListener, ListSelectionListener {
    //Campos
    Vista vista;
    Modelo modelo;

    /**
     * Constructor de Controlador
     * @param vista de tipo Vista
     * @param modelo de tipo Modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        inicializar();
    }

    /**
     * Método inicializar listeners
     */
    private void inicializar() {
        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);
        modelo.conectar();
        listarJuguete(modelo.getJuguete());
        listarJugueteria(modelo.getJugueteria());
        listarProveedor(modelo.getProveedor());
    }

    /**
     * Método addActionListeners(), añado los listeners a los botones
     * @param listener de tipo ActionListener
     */
    private void addActionListeners(ActionListener listener){
        vista.btnnuevoJuguete.addActionListener(listener);
        vista.btnnuevaJugueteria.addActionListener(listener);
        vista.btnnuevoProveedor.addActionListener(listener);
        vista.btnmodificarJuguete.addActionListener(listener);
        vista.btnmodificarJugueteria.addActionListener(listener);
        vista.btnmodificarProveedor.addActionListener(listener);
        vista.btneliminarJuguete.addActionListener(listener);
        vista.btneliminarJugueteria.addActionListener(listener);
        vista.btneliminarProveedor.addActionListener(listener);


    }

    /**
     * Método addListSelectionListeners(), añadir listeners a los JLists
     * @param listener de ListSelectionListener
     */
    private void addListSelectionListeners(ListSelectionListener listener){
        vista.listJuguete.addListSelectionListener(listener);
        vista.listJugueteria.addListSelectionListener(listener);
        vista.listProveedor.addListSelectionListener(listener);
    }

    /**
     * Método addKeyListeners(), añadir listeners a los campos de buscar
     * @param listener de KeyListener
     */
    private void addKeyListeners(KeyListener listener){
        vista.txtBuscarJuguete.addKeyListener(listener);
        vista.txtBuscarJugueteSimple.addKeyListener(listener);
        vista.txtBuscarJugueteria.addKeyListener(listener);
        vista.txtBuscarJugueteriaSimple.addKeyListener(listener);
        vista.txtBuscarProveedor.addKeyListener(listener);
        vista.txtBuscarProveedorSimple.addKeyListener(listener);


    }


    /**
     * Método actionPerformed(), implementado por ActionListener, donde asignas las acciones a los botones
     * @param e de ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        Juguete unJuguete;
        Jugueteria unaJugueteria;
        Proveedor unProveedor;
        switch (comando){
            case "Nuevo Juguete":
                unJuguete = new Juguete();
                modificarJugueteFromCampos(unJuguete);
                modelo.guardarJuguete(unJuguete);
                listarJuguete(modelo.getJuguete());
                break;
            case "Modificar Juguete":
                unJuguete = (Juguete) vista.listJuguete.getSelectedValue();
                modificarJugueteFromCampos(unJuguete);
                modelo.modificarJuguete(unJuguete);

                listarJuguete(modelo.getJuguete());
                break;
            case "Eliminar Juguete":
                unJuguete = (Juguete) vista.listJuguete.getSelectedValue();
                modelo.borrarJuguete(unJuguete);
                listarJuguete(modelo.getJuguete());
                break;
            case "Nueva Jugueteria":
                unaJugueteria = new Jugueteria();
                modificarJugueteriaFromCampos(unaJugueteria);
                modelo.guardarJugueteria(unaJugueteria);
                listarJugueteria(modelo.getJugueteria());
                break;
            case "Modificar Jugueteria":
                unaJugueteria = (Jugueteria) vista.listJugueteria.getSelectedValue();
                modificarJugueteriaFromCampos(unaJugueteria);
                modelo.modificarJugueteria(unaJugueteria);

                listarJugueteria(modelo.getJugueteria());
                break;
            case "Eliminar Jugueteria":
                unaJugueteria = (Jugueteria) vista.listJugueteria.getSelectedValue();
                modelo.borrarJugueteria(unaJugueteria);
                listarJugueteria(modelo.getJugueteria());
                break;
            case "Nuevo Proveedor":
                unProveedor = new Proveedor();
                modificarProveedorFromCampos(unProveedor);
                modelo.guardarProveedor(unProveedor);
                listarProveedor(modelo.getProveedor());
                break;
            case "Modificar Proveedor":
                unProveedor = (Proveedor) vista.listProveedor.getSelectedValue();
                modificarProveedorFromCampos(unProveedor);
                modelo.modificarProveedor(unProveedor);

                listarProveedor(modelo.getProveedor());
                break;
            case "Eliminar Proveedor":
                unProveedor = (Proveedor) vista.listProveedor.getSelectedValue();
                modelo.borrarProveedor(unProveedor);
                listarProveedor(modelo.getProveedor());
                break;
        }
    }

    /**
     * Método listarJuguete(), listo los juguetes
     * @param lista de List<Juguete>
     */
    private void listarJuguete(List<Juguete> lista){
        vista.dlmJuguete.clear();
        for (Juguete juguete : lista){
            vista.dlmJuguete.addElement(juguete);


        }
    }
    /**
     * Método listarJugueteria(), listo las Jugueterias
     * @param lista de List<Jugueteria>
     */
    private void listarJugueteria(List<Jugueteria> lista){
        vista.dlmJugueteria.clear();
        for (Jugueteria jugueteria : lista){
            vista.dlmJugueteria.addElement(jugueteria);




        }
    }
    /**
     * Método listarProveedor(), listo los proveedores
     * @param lista de List<Proveedor>
     */
    private void listarProveedor(List<Proveedor> lista){
        vista.dlmProveedor.clear();
        for (Proveedor proveedor : lista){
            vista.dlmProveedor.addElement(proveedor);
        }
    }

    /**
     * Método modificarJugueteFromCampos(), para modificar los datos
     * @param unJuguete de tipo Juguete
     */
    private void modificarJugueteFromCampos(Juguete unJuguete) {
        unJuguete.setCodigoJuguete(vista.txtCodigoJuguete.getText());
        unJuguete.setNombreJuguete(vista.txtNombreJuguete.getText());
        unJuguete.setDescripcionJuguete(vista.txtDescripcionJuguete.getText());
        unJuguete.setMarcaJuguete(vista.txtMarcaJuguete.getText());
        unJuguete.setPrecioJuguete(Double.parseDouble(vista.txtPrecioJuguete.getText()));
        unJuguete.setFechaCreacionJuguete(vista.datePickerJuguete.getDate());

    }
    /**
     * Método modificarJugueteriaFromCampos(), para modificar los datos
     * @param unaJugueteria de tipo Jugueteria
     */
    private void modificarJugueteriaFromCampos(Jugueteria unaJugueteria) {
        unaJugueteria.setNombreJugueteria(vista.txtNombreJugueteria.getText());
        unaJugueteria.setCiudadJugueteria(vista.txtCiudadJugueteria.getText());
        unaJugueteria.setPaisJugueteria(vista.txtPaisJugueteria.getText());
        unaJugueteria.setTelefonoJugueteria(vista.txtTelefonoJugueteria.getText());
    }
    /**
     * Método modificarProveedorFromCampos(), para modificar los datos
     * @param unProveedor de tipo Proveedor
     */
    private void modificarProveedorFromCampos(Proveedor unProveedor) {
        unProveedor.setCodigoProveedor(vista.txtCodigoProveedor.getText());
        unProveedor.setPaisProveedor(vista.txtPaisProveedor.getText());
        unProveedor.setCiudadProveedor(vista.txtCiudadProveedor.getText());
        unProveedor.setDireccionProveedor(vista.txtDireccionProveedor.getText());
        unProveedor.setCantidadProveedor(Integer.parseInt(vista.txtCantidadProveedor.getText()));
        unProveedor.setTelefonoProveedor(vista.txtTelefonoProveedor.getText());
    }

    /**
     * Método valueChanged(), que al pinchar en un registro, me muestre sus datos en sus campos
     * @param e de ListSelectionEvent
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == vista.listJuguete) {
            if (vista.listJuguete.getSelectedValue() != null) {
                Juguete juguete = (Juguete) vista.listJuguete.getSelectedValue();
                vista.txtCodigoJuguete.setText(juguete.getCodigoJuguete());
                vista.txtNombreJuguete.setText(juguete.getNombreJuguete());
                vista.txtDescripcionJuguete.setText(juguete.getDescripcionJuguete());
                vista.txtMarcaJuguete.setText(juguete.getMarcaJuguete());
                vista.txtPrecioJuguete.setText(String.valueOf(juguete.getPrecioJuguete()));
                vista.datePickerJuguete.setDate(juguete.getFechaCreacionJuguete());

            }
        } else if (e.getSource() == vista.listJugueteria) {
            if (vista.listJugueteria.getSelectedValue() != null) {
                Jugueteria jugueteria = (Jugueteria) vista.listJugueteria.getSelectedValue();
                vista.txtNombreJugueteria.setText(jugueteria.getNombreJugueteria());
                vista.txtCiudadJugueteria.setText(jugueteria.getCiudadJugueteria());
                vista.txtPaisJugueteria.setText(jugueteria.getPaisJugueteria());
                vista.txtTelefonoJugueteria.setText(jugueteria.getTelefonoJugueteria());
            }
        } else if (e.getSource() == vista.listProveedor) {
            if (vista.listProveedor.getSelectedValue() != null) {
                Proveedor proveedor = (Proveedor) vista.listProveedor.getSelectedValue();
                vista.txtCodigoProveedor.setText(proveedor.getCodigoProveedor());
                vista.txtPaisProveedor.setText(proveedor.getPaisProveedor());
                vista.txtCiudadProveedor.setText(proveedor.getCiudadProveedor());
                vista.txtDireccionProveedor.setText(proveedor.getDireccionProveedor());
                vista.txtCantidadProveedor.setText(String.valueOf(proveedor.getCantidadProveedor()));
                vista.txtTelefonoProveedor.setText(proveedor.getTelefonoProveedor());

            }
        }
    }

    /**
     * Método keyReleased(), para listar según lo que buscas
     * @param e de KeyEvent
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscarJuguete) {
            listarJuguete(modelo.getJuguete(vista.txtBuscarJuguete.getText()));
            if (vista.txtBuscarJuguete.getText().isEmpty()) {
                vista.dlmJuguete.clear();
            }

        } else if (e.getSource() == vista.txtBuscarJugueteSimple) {
            listarJuguete(modelo.getJugueteSimple(vista.txtBuscarJugueteSimple.getText()));
            if (vista.txtBuscarJugueteSimple.getText().isEmpty()) {
                vista.dlmJuguete.clear();
            }
        }else if (e.getSource() == vista.txtBuscarJugueteria) {
                listarJugueteria(modelo.getJugueteria(vista.txtBuscarJugueteria.getText()));
                if (vista.txtBuscarJugueteria.getText().isEmpty()) {
                    vista.dlmJugueteria.clear();
                }
        } else if (e.getSource() == vista.txtBuscarJugueteriaSimple) {
            listarJugueteria(modelo.getJugueteriaSimple(vista.txtBuscarJugueteriaSimple.getText()));
            if (vista.txtBuscarJugueteriaSimple.getText().isEmpty()) {
                vista.dlmJugueteria.clear();
            }
        }else if (e.getSource() == vista.txtBuscarProveedor) {
            listarProveedor(modelo.getProveedor(vista.txtBuscarProveedor.getText()));
            if (vista.txtBuscarProveedor.getText().isEmpty()) {
                vista.dlmProveedor.clear();
            }
        } else if (e.getSource() == vista.txtBuscarProveedorSimple) {
            listarProveedor(modelo.getProveedorSimple(vista.txtBuscarProveedorSimple.getText()));
            if (vista.txtBuscarProveedorSimple.getText().isEmpty()) {
                vista.dlmProveedor.clear();
            }
        }



    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }




}
