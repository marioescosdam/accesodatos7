package com.marioea.mongo.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.marioea.mongo.base.Juguete;
import com.marioea.mongo.base.Jugueteria;
import com.marioea.mongo.base.Proveedor;

import javax.swing.*;

/**
 * Author: Mario Escos
 */
public class Vista {
 /**
  * Elementos de nuestra
  */
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
     JTextField txtCodigoJuguete;
     JTextField txtNombreJuguete;
     JTextField txtDescripcionJuguete;
     JTextField txtMarcaJuguete;
     JTextField txtPrecioJuguete;
     DatePicker datePickerJuguete;
     JButton btnnuevoJuguete;
     JButton btnmodificarJuguete;
     JButton btneliminarJuguete;
     JTextField txtBuscarJuguete;
     JList listJuguete;
     JTextField txtNombreJugueteria;
     JTextField txtCiudadJugueteria;
     JTextField txtPaisJugueteria;
     JTextField txtTelefonoJugueteria;
     JTextField txtBuscarJugueteria;
     JButton btnnuevaJugueteria;
     JButton btnmodificarJugueteria;
     JButton btneliminarJugueteria;
     JList listJugueteria;
     JTextField txtCodigoProveedor;
     JTextField txtNombreProveedor;
     JTextField txtPaisProveedor;
     JTextField txtCiudadProveedor;
     JTextField txtDireccionProveedor;
     JTextField txtCantidadProveedor;
     JTextField txtTelefonoProveedor;
     JTextField txtBuscarProveedor;
     JButton btnnuevoProveedor;
     JButton btnmodificarProveedor;
     JButton btneliminarProveedor;
     JList listProveedor;
     JTextField txtBuscarProveedorSimple;
     JTextField txtBuscarJugueteriaSimple;
     JTextField txtBuscarJugueteSimple;


    DefaultListModel<Juguete> dlmJuguete;
    DefaultListModel<Jugueteria> dlmJugueteria;
    DefaultListModel<Proveedor> dlmProveedor;


 /**
  * Constructor de Vista()
  */
 public Vista() {
  JFrame frame = new JFrame("AlmacenJugueteria");
  frame.setContentPane(panel1);
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  frame.pack();
  frame.setVisible(true);

  inicializar();
 }

 /**
  * inicializar(),inicializar modelos
  */
 private void inicializar(){
  dlmJuguete = new DefaultListModel<Juguete>();
  listJuguete.setModel(dlmJuguete);

  dlmJugueteria = new DefaultListModel<Jugueteria>();
  listJugueteria.setModel(dlmJugueteria);
  dlmProveedor = new DefaultListModel<Proveedor>();
  listProveedor.setModel(dlmProveedor);

  datePickerJuguete.getComponentToggleCalendarButton().setText("Calendario");


 }


}

