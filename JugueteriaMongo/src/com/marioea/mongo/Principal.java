package com.marioea.mongo;
import com.marioea.mongo.gui.Controlador;
import com.marioea.mongo.gui.Modelo;
import com.marioea.mongo.gui.Vista;


/**
 * Clase Principal
 * @author Mario Escos
 */
public class Principal {
    /**
     * Método main(), es estatico e inicia a la aplicacion
     * @param args de tipo String[]
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
