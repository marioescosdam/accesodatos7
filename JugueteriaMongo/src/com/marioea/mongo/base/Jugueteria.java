package com.marioea.mongo.base;

import org.bson.types.ObjectId;

/**
 * @author Mario Escos
 */
public class Jugueteria {
        //Campos de la clase Jugueteria
        private ObjectId id;
        private String nombreJugueteria;
        private String ciudadJugueteria;
        private String paisJugueteria;
        private String telefonoJugueteria;

    /**
     * constructor vacio de la clase jugueteria
     */
    public Jugueteria() {

    }

    /**
     * creacion de getters y setters de la clase Jugueteria
     * @return
     */
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombreJugueteria() {
        return nombreJugueteria;
    }

    public void setNombreJugueteria(String nombreJugueteria) {
        this.nombreJugueteria = nombreJugueteria;
    }

    public String getCiudadJugueteria() {
        return ciudadJugueteria;
    }

    public void setCiudadJugueteria(String ciudadJugueteria) {
        this.ciudadJugueteria = ciudadJugueteria;
    }

    public String getPaisJugueteria() {
        return paisJugueteria;
    }

    public void setPaisJugueteria(String paisJugueteria) {
        this.paisJugueteria = paisJugueteria;
    }

    public String getTelefonoJugueteria() {
        return telefonoJugueteria;
    }

    public void setTelefonoJugueteria(String telefonoJugueteria) {
        this.telefonoJugueteria = telefonoJugueteria;
    }

    /**
     * metodo toString de la clase Jugueteria
     * @return
     */
    @Override
    public String toString() {
        return
                "nombreJugueteria='" + nombreJugueteria + '\'' +
                ", ciudadJugueteria='" + ciudadJugueteria + '\'' +
                ", paisJugueteria='" + paisJugueteria + '\'' +
                ", telefonoJugueteria='" + telefonoJugueteria + '\''
                ;
    }



}
