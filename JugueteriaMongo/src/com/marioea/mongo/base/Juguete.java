package com.marioea.mongo.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

/**
 * @author Mario Escos
 */
public class Juguete {
    //Campos de la clase Juguete
    private ObjectId id;
    private String codigoJuguete;
    private String nombreJuguete;
    private String descripcionJuguete;
    private String marcaJuguete;
    private double precioJuguete;
    private LocalDate fechaCreacionJuguete;

    /**
     * Constructor vacio de la clase juguete
     */
    public Juguete() {

    }

    /**
     * getters y setters de la clase juguete
     * @return
     */
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getCodigoJuguete() {
        return codigoJuguete;
    }

    public void setCodigoJuguete(String codigoJuguete) {
        this.codigoJuguete = codigoJuguete;
    }

    public String getNombreJuguete() {
        return nombreJuguete;
    }

    public void setNombreJuguete(String nombreJuguete) {
        this.nombreJuguete = nombreJuguete;
    }

    public String getDescripcionJuguete() {
        return descripcionJuguete;
    }

    public void setDescripcionJuguete(String descripcionJuguete) {
        this.descripcionJuguete = descripcionJuguete;
    }

    public String getMarcaJuguete() {
        return marcaJuguete;
    }

    public void setMarcaJuguete(String marcaJuguete) {
        this.marcaJuguete = marcaJuguete;
    }

    public double getPrecioJuguete() {
        return precioJuguete;
    }

    public void setPrecioJuguete(double precioJuguete) {
        this.precioJuguete = precioJuguete;
    }

    public LocalDate getFechaCreacionJuguete() {
        return fechaCreacionJuguete;
    }

    public void setFechaCreacionJuguete(LocalDate fechaCreacionJuguete) {
        this.fechaCreacionJuguete = fechaCreacionJuguete;
    }


    /**
     * metodo to String
     * @return
     */
    @Override
    public String toString() {
        Jugueteria jugueteria1= new Jugueteria();

        return
                "  codigoJuguete='" + codigoJuguete + '\'' +
                ", nombreJuguete='" + nombreJuguete + '\'' +
                ", descripcionJuguete='" + descripcionJuguete + '\'' +
                ", marcaJuguete='" + marcaJuguete + '\'' +
                ", precioJuguete=" + precioJuguete +
                ", fechaCreacionJuguete=" + fechaCreacionJuguete
                ;
    }
}
