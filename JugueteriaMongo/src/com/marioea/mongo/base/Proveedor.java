package com.marioea.mongo.base;

import org.bson.types.ObjectId;

/**
 * @author Mario Escos
 */
public class Proveedor {
    private ObjectId id;
    private String codigoProveedor;
    private String paisProveedor;
    private String ciudadProveedor;
    private String direccionProveedor;
    private int cantidadProveedor;
    private String telefonoProveedor;

    /**
     * constructor vacio de la clase proveedor
     */
    public Proveedor() {

    }

    /**
     * generamos los getters y setters de la clase proveedor
     * @return
     */
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getCodigoProveedor() {
        return codigoProveedor;
    }

    public void setCodigoProveedor(String codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }

    public String getPaisProveedor() {
        return paisProveedor;
    }

    public void setPaisProveedor(String paisProveedor) {
        this.paisProveedor = paisProveedor;
    }

    public String getCiudadProveedor() {
        return ciudadProveedor;
    }

    public void setCiudadProveedor(String ciudadProveedor) {
        this.ciudadProveedor = ciudadProveedor;
    }

    public String getDireccionProveedor() {
        return direccionProveedor;
    }

    public void setDireccionProveedor(String direccionProveedor) {
        this.direccionProveedor = direccionProveedor;
    }

    public int getCantidadProveedor() {
        return cantidadProveedor;
    }

    public void setCantidadProveedor(int cantidadProveedor) {
        this.cantidadProveedor = cantidadProveedor;
    }

    public String getTelefonoProveedor() {
        return telefonoProveedor;
    }

    public void setTelefonoProveedor(String telefonoProveedor) {
        this.telefonoProveedor = telefonoProveedor;
    }

    /**
     * generamos el metodo to String de la clase Proveedor
     * @return
     */
    @Override
    public String toString() {
        return
                ", codigoProveedor='" + codigoProveedor + '\'' +
                ", paisProveedor='" + paisProveedor + '\'' +
                ", ciudadProveedor='" + ciudadProveedor + '\'' +
                ", direccionProveedor='" + direccionProveedor + '\'' +
                ", cantidadProveedor=" + cantidadProveedor +
                ", telefonoProveedor='" + telefonoProveedor + '\''
                ;
    }
}
