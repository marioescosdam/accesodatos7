package com.marioea.mongo.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Mario Escos
 * @author
 */

/**
 * Clase Util
 */
public class Util {
    /**
     * Método formatearFecha(), formatea la fecha
     * @param fecha
     * @return formateador.format(fecha);
     */
    public static String formatearFecha(LocalDate fecha) {
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return formateador.format(fecha);
    }

    /**
     * Método parsearFecha() de tipo LocalDate
     * @param fecha de tipo String
     * @return LocalDate.parse(fecha, formateador)
     */
    public static LocalDate parsearFecha(String fecha){
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return LocalDate.parse(fecha, formateador);
    }
}
